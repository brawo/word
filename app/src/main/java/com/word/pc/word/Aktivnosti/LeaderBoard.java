package com.word.pc.word.Aktivnosti;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.kosalgeek.android.json.JsonConverter;
import com.word.pc.word.Adapteri.RecyclerAvatar;
import com.word.pc.word.Adapteri.RecyclerLeader;
import com.word.pc.word.Helperi.MySingleton;
import com.word.pc.word.Misc.RangCalculator;
import com.word.pc.word.Misc.Utils;
import com.word.pc.word.Modeli.User;
import com.word.pc.word.Modeli.Word;
import com.word.pc.word.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LeaderBoard extends AppCompatActivity {
    RecyclerView rvLeader;
    RecyclerView.LayoutManager layoutManager;
    RecyclerLeader adapter;
    Context context;
    ArrayList<User> arrayList = new ArrayList<>();
    String url_details = "http://lazyiguanastudios.com/webservices/Wordinary/getAllUsers.php";
    String url_find_rank = "http://lazyiguanastudios.com/webservices/Wordinary/findMyRang.php";
    TextView tvNaslovLeader,tvPlace,tvScore,tvNastavak,tvNastavak2;
    ImageView ivProfileLeader;
    private String prebacenScore,prebacenUsername;
    public static String prebacenID;
    private String resultRank;
    private String prebacenAvatar;
    private Drawable avatar;
    Typeface quick_bold,quick_light,quick_medium,quick_regular;
    View viewLeader;


    public void init(){
        viewLeader = findViewById(R.id.viewLeader);

        rvLeader = findViewById(R.id.rvLeaderBoard);
        rvLeader.addItemDecoration(new DividerItemDecoration(rvLeader.getContext(), DividerItemDecoration.VERTICAL));
        layoutManager = new LinearLayoutManager(this);
        rvLeader.setLayoutManager(layoutManager);
        rvLeader.setHasFixedSize(true);
        //========================================================================================== rv optimizacija
        rvLeader.setItemViewCacheSize(20);
        rvLeader.setDrawingCacheEnabled(true);
        rvLeader.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        //==========================================================================================
        adapter = new RecyclerLeader(arrayList, this);
        adapter.setHasStableIds(true);
        rvLeader.setAdapter(adapter);

        context = this;
        tvNastavak = findViewById(R.id.tvNastavak);
        tvNastavak2 = findViewById(R.id.tvNastavak2);
        tvNaslovLeader = findViewById(R.id.tvNaslovLeader);
        tvPlace = findViewById(R.id.tvPlace);
        tvScore = findViewById(R.id.tvScore);
        ivProfileLeader = findViewById(R.id.ivProfileLeader);
        if(Utils.TEMA == 1){
            viewLeader.setBackground(context.getResources().getDrawable(R.drawable.gradient2_night_mode));

        }else {
            viewLeader.setBackground(context.getResources().getDrawable(R.drawable.gradient2));
        }

        //----------------------------------------------------
        prebacenUsername = getIntent().getStringExtra("username");
        prebacenAvatar = getIntent().getStringExtra("avatar");
        prebacenScore = getIntent().getStringExtra("score");
        prebacenID = getIntent().getStringExtra("id");
        //----------------------------------------------------
        tvScore.setText(prebacenScore);

        String pocetakA = "avatars/";
        String slika = pocetakA.concat(prebacenAvatar).concat(".png");


        try {
            avatar = Drawable.createFromStream(context.getAssets().open(slika), null);
        } catch (IOException e) {
            e.printStackTrace();
        }


        Glide
                .with(context)
                .load(avatar)
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                .apply(RequestOptions.skipMemoryCacheOf(true))
                .apply( new RequestOptions().optionalCircleCrop())
                .into(ivProfileLeader);

        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------ fontovi
        quick_bold = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Bold.ttf");
        quick_light = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Light.ttf");
        quick_medium = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Medium.ttf");
        quick_regular = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Regular.ttf");

        tvNaslovLeader.setTypeface(quick_regular);
        tvNastavak.setTypeface(quick_light);
        tvNastavak2.setTypeface(quick_light);
        tvScore.setTypeface(quick_medium);
        tvPlace.setTypeface(quick_medium);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Utils.TEMA == 0){
            setTheme(R.style.AppTheme);
        }else {
            setTheme(R.style.NightProfile);
        }
        setContentView(R.layout.activity_leader_board);
        getSupportActionBar().hide();
        init();
        userDetails();
        findMyRank();

    }

    private void userDetails() {

        StringRequest stringRequest2 = new StringRequest(Request.Method.POST, url_details, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                ArrayList<User> latestArrayList = new JsonConverter<User>()
                        .toArrayList(response, User.class);

                adapter = new RecyclerLeader(latestArrayList,getApplicationContext());

                rvLeader.setAdapter(adapter);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Something went wrong, try again...", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("english",etGermanS.getText().toString());
                //params.put("id", prebacenID);
                return params;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest2);

    }

    private void findMyRank() {

        StringRequest stringRequest2 = new StringRequest(Request.Method.POST, url_find_rank, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jsonObject = new JSONObject(response.toString());

                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    JSONObject jsonRow = jsonArray.getJSONObject(0);

                    resultRank = jsonRow.getString("rank");
                    if(resultRank.equals("1")){
                        tvNastavak.setText("st");
                    }else if(resultRank.equals("2")){
                        tvNastavak.setText("nd");
                    }else {
                        tvNastavak.setText("th");
                    }
                    tvPlace.setText(resultRank);
                    tvNastavak2.setText("pts");


                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Something went wrong, try again...", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", String.valueOf(Utils.USER_ID));
                return params;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest2);

    }
}
