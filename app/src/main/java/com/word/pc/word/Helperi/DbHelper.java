package com.word.pc.word.Helperi;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.word.pc.word.Misc.DbContract;

public class DbHelper extends SQLiteOpenHelper {

    public DbHelper(Context context) {
        super(context, DbContract.DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static final int DATABASE_VERSION = 1;

    public static final String CREATE_TABLE = "CREATE TABLE words (\n" +
            " id integer PRIMARY KEY,\n" +
            " word text NOT NULL,\n" +
            " translate text NOT NULL,\n" +
            " user_id integer NOT NULL,\n" +
            " syncstatus integer NOT NULL);";
    public static final String DROP_TABLE = "drop table if exists "+DbContract.TABLE_NAME;

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL(DROP_TABLE);
        onCreate(db);

    }

    public void SaveToLocalDatabase(String word,String translate,int user_id,int sync_status,SQLiteDatabase database)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DbContract.WORD,word);
        contentValues.put(DbContract.TRANSLATE,translate);
        contentValues.put(DbContract.USER_ID,user_id);
        contentValues.put(DbContract.SYNC_STATUS,sync_status);
        database.insert(DbContract.TABLE_NAME,null,contentValues);
    }

    public Cursor readFromLocalDatbase(SQLiteDatabase database)
    {
        String[] projection = {DbContract.WORD,DbContract.TRANSLATE,DbContract.USER_ID,DbContract.SYNC_STATUS};
        return (database.query(DbContract.TABLE_NAME,projection,null,null,null,null,null));

        //database.rawQuery("SELECT word FROM words WHERE syncstatus = 1", null);


    }

    public int readWordNotOnServer(SQLiteDatabase database){
        int nombr = 0;
        Cursor cursor = database.rawQuery("SELECT word FROM words WHERE syncstatus = 1", null);
        nombr = cursor.getCount();
        return nombr;

    }

    public void updateLocalDatabase(String word,String translate,int user_id,int sync_status,SQLiteDatabase database)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DbContract.SYNC_STATUS,sync_status);
        contentValues.put(DbContract.TRANSLATE,translate);
        contentValues.put(DbContract.USER_ID,user_id);
        String selection = DbContract.WORD+" = ?";
        String[] selection_args = {word};
        database.update(DbContract.TABLE_NAME,contentValues,selection,selection_args);
    }
}