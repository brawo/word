package com.word.pc.word.Aktivnosti;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.word.pc.word.Adapteri.RecyclerAdapter;
import com.word.pc.word.Adapteri.RecyclerAvatar;
import com.word.pc.word.Adapteri.ViewPagerAdapter;
import com.word.pc.word.Helperi.MySingleton;
import com.word.pc.word.Misc.RangCalculator;
import com.word.pc.word.Misc.Utils;
import com.word.pc.word.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Proile extends AppCompatActivity {
    RecyclerView rvAvatar;
    RecyclerView.LayoutManager layoutManager;
    RecyclerAvatar adapter;
    TextView tvFullName, tvScore, tvTrophy;
    ImageButton ibScore, ibTrophy, ibArrow;
    ImageView view6;
    public static TextView tvPrebacenAvatar;
    public static Button btChangeAvatar;
    public static ImageView ivProfileP, ivBadgeP;
    private ConstraintSet layout1, layout2;
    private ConstraintLayout constraintLayout;
    private boolean isOpen = false;
    private Drawable avatar,badge;
    public static int prebacenAvatarInt;
    Context context;
    Typeface quick_bold,quick_light,quick_medium,quick_regular;
    String url_update_avatar = "http://lazyiguanastudios.com/webservices/Wordinary/updateAvatar.php";
    View viewCover;
    RelativeLayout relativeLayoutProfile;

    //----------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------

    String prebacenoIme,prebacenoPrezime,prebacenRang,prebacenScore,prebacenAvatar;
    int prebacenRangInt,prebacenScoreInt;

    //----------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------- prebaceno iz Main

    String[] avatari = {"1", "2", "3", "4","5","6","7","8","9","10",
                        "11", "12","13","14","15","16","17","18","19", "20",
                        "21", "22","23","24","25","26","27","28","29", "30",
                        "31", "32","33","34","35","36","37","38","39", "40",
                        "41", "42","43","44","45","46","47","48","49", "50",
                        "51", "52","53","54","55","56","57","58","59", "60",
                        "61", "62","63","64","65","66","67","68","69", "70",
                        "71", "72","73","74","75","76","77","78","79", "80",
                        "81", "82","83","84","85","86","87","88","89", "90",
                        "91", "92","93","94","95","96","97","98","99", "100",
                        "101", "102","103","104","105","106","107","108","109", "110",
                        "111", "112","113","114","115","116","117","118","119", "120"};

    public void init() {

        //----------------------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------------
        prebacenoIme = getIntent().getStringExtra("name");
        prebacenoPrezime = getIntent().getStringExtra("lastname");
        prebacenRang = getIntent().getStringExtra("rang");
        prebacenScore = getIntent().getStringExtra("score");
        prebacenAvatar = getIntent().getStringExtra("avatar");

        prebacenRangInt = Integer.parseInt(prebacenRang);
        prebacenAvatarInt= Integer.parseInt(prebacenAvatar);

        //----------------------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------------

        ArrayList list = new ArrayList();
        for(int i = 0;i<=prebacenRangInt*5;i++){
            String avatariFinal = avatari[i];
            list.addAll(Arrays.asList(avatariFinal));

        }
        ArrayList<String> lista2 = new ArrayList<String>(list);


        layout1 = new ConstraintSet();
        layout2 = new ConstraintSet();
        constraintLayout = (ConstraintLayout) findViewById(R.id.constraint_layoutP);
        layout2.clone(this, R.layout.profile_extended);
        layout1.clone(constraintLayout);
        context = this;
        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------ animacija layout-a

        viewCover = findViewById(R.id.viewCover);
        relativeLayoutProfile = findViewById(R.id.relativeLayoutProfile);
        if(Utils.TEMA == 1){
            viewCover.setBackground(context.getResources().getDrawable(R.drawable.gradient_night_mode));
            constraintLayout.setBackgroundColor(context.getResources().getColor(R.color.crna));
            relativeLayoutProfile.setBackground(context.getResources().getDrawable(R.drawable.placeholder_night_mode));
        }else {
            viewCover.setBackground(context.getResources().getDrawable(R.drawable.gradient));
            constraintLayout.setBackgroundColor(context.getResources().getColor(R.color.svetloLjubicasta));
            relativeLayoutProfile.setBackground(context.getResources().getDrawable(R.drawable.placeholder));
        }
        view6 = findViewById(R.id.view6);
        tvPrebacenAvatar = findViewById(R.id.tvPrebacenAvatar);
        btChangeAvatar = findViewById(R.id.btChangeAvatar);
        ivProfileP = findViewById(R.id.ivProfileP);
        ivBadgeP = findViewById(R.id.ivBadgeP);
        tvFullName = findViewById(R.id.tvFullName);
        tvScore = findViewById(R.id.tvScore);
        tvTrophy = findViewById(R.id.tvTrophy);
        ibScore = findViewById(R.id.ibScore);
        ibTrophy = findViewById(R.id.ibTrophy);
        ibArrow = findViewById(R.id.ibArrow);
        rvAvatar = findViewById(R.id.rvAvatar);
         //========================================================================================== rv optimizacija
        rvAvatar.setItemViewCacheSize(20);
        rvAvatar.setDrawingCacheEnabled(true);
        rvAvatar.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        //==========================================================================================

        //layoutManager = new LinearLayoutManager(this);
        layoutManager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        rvAvatar.setLayoutManager(layoutManager);
        rvAvatar.setHasFixedSize(true);
        adapter = new RecyclerAvatar(lista2, this);
        adapter.setHasStableIds(true);
        rvAvatar.setAdapter(adapter);
        context = this;

        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------ init
        tvFullName.setText(prebacenoIme);
        tvTrophy.setText(prebacenRang);
        tvScore.setText(prebacenScore);

        String pocetakA = "avatars/";
        String pocetakB = "badges/";
        String slika = pocetakA.concat(prebacenAvatar).concat(".png");
        String slikaBadge = pocetakB.concat(prebacenRang).concat(".png");

        try {
            avatar = Drawable.createFromStream(context.getAssets().open(slika), null);
            badge = Drawable.createFromStream(context.getAssets().open(slikaBadge), null);
        } catch (IOException e) {
            e.printStackTrace();
        }


        Glide
                .with(context)
                .load(R.drawable.output)
                .apply( new RequestOptions().placeholder(R.drawable.output))
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                .apply(RequestOptions.skipMemoryCacheOf(true))
                .into(view6);

        Glide
                .with(context)
                .load(avatar)
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                .apply(RequestOptions.skipMemoryCacheOf(true))
                .apply( new RequestOptions().optionalCircleCrop())
                .apply( new RequestOptions().placeholder(R.drawable.user2).optionalCircleCrop())
                .into(ivProfileP);

        Glide
                .with(context)
                .load(badge)
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                .apply(RequestOptions.skipMemoryCacheOf(true))
                //.apply( new RequestOptions().placeholder(R.drawable.placeholder))
                .into(ivBadgeP);

        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------ fontovi
        quick_bold = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Bold.ttf");
        quick_light = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Light.ttf");
        quick_medium = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Medium.ttf");
        quick_regular = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Regular.ttf");

        tvFullName.setTypeface(quick_medium);
        tvScore.setTypeface(quick_regular);
        tvTrophy.setTypeface(quick_regular);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Utils.TEMA == 0){
        setTheme(R.style.AppTheme);
        }else {
            setTheme(R.style.NightProfile);
        }
        setContentView(R.layout.activity_proile);
        getSupportActionBar().hide();
        init();

        ibArrow.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View v) {
                if (!isOpen) {
                    TransitionManager.beginDelayedTransition(constraintLayout);
                    layout2.applyTo(constraintLayout);
                    isOpen = !isOpen;
                    ibArrow.animate().rotation(180f).setDuration(500);
                    //ibArrow.setImageDrawable(getResources().getDrawable(R.drawable.arrow_down));

                } else {
                    TransitionManager.beginDelayedTransition(constraintLayout);
                    layout1.applyTo(constraintLayout);
                    isOpen = !isOpen;
                    //ibArrow.animate().rotation(-180f).setDuration(500);
                    //ibArrow.setImageDrawable(getResources().getDrawable(R.drawable.arrow_up));
                }
            }
        });

        btChangeAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateAvatar();
            }
        });
    }

    public void updateAvatar() {


        StringRequest stringRequest2 = new StringRequest(Request.Method.POST, url_update_avatar, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                btChangeAvatar.setVisibility(View.INVISIBLE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Something went wrong, try again...",Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("avatar",tvPrebacenAvatar.getText().toString());
                params.put("id", String.valueOf(Utils.USER_ID));
                return params;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest2);

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
