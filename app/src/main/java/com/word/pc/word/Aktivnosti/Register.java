package com.word.pc.word.Aktivnosti;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.schibsted.spain.parallaxlayerlayout.ParallaxLayerLayout;
import com.schibsted.spain.parallaxlayerlayout.SensorTranslationUpdater;
import com.word.pc.word.Helperi.MySingleton;
import com.word.pc.word.Misc.Utils;
import com.word.pc.word.R;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import static com.word.pc.word.Misc.RangCalculator.proper_noun;
import static com.word.pc.word.Misc.RangCalculator.proper_noun2;

public class Register extends AppCompatActivity {


    //String url_register = "http://lazyiguanastudios.com/webservices/Wordinary/register.php";
    String url_register = "http://lazyiguanastudios.com/webservices/Wordinary/register2.php";
    TextView tvNaslovR,tvGenerate;
    EditText etUsernameR,etPasswordR,etNameR,etLastnameR;
    Button btSignUp;
    Typeface quick_bold,quick_light,quick_medium,quick_regular;
    HorizontalScrollView horizontalScrollView3;
    String concatMail;
    ImageView view6;
    ConstraintLayout register_layout;
    AlertDialog dialog;

    public void init(){
        register_layout = findViewById(R.id.register_layout);
        view6 = findViewById(R.id.view6);
        tvNaslovR = findViewById(R.id.tvNaslovR);
        tvGenerate = findViewById(R.id.tvGenerate);
        etNameR = findViewById(R.id.etNameR);
        etLastnameR = findViewById(R.id.etLastnameR);
        etUsernameR = findViewById(R.id.etUsernameR);
        etPasswordR = findViewById(R.id.etPasswordR);
        btSignUp = findViewById(R.id.btSignUp);
        horizontalScrollView3 = findViewById(R.id.horizontalScrollView3);


        if(Utils.TEMA == 1){
            register_layout.setBackgroundColor(getResources().getColor(R.color.crna));
            btSignUp.setBackground(getResources().getDrawable(R.drawable.button_back_night_mode));
        }else {
            btSignUp.setBackground(getResources().getDrawable(R.drawable.button_back_white_white_stroke));
        }
        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------ fontovi
        quick_bold = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Bold.ttf");
        quick_light = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Light.ttf");
        quick_medium = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Medium.ttf");
        quick_regular = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Regular.ttf");

        tvNaslovR.setTypeface(quick_light);
        tvGenerate.setTypeface(quick_regular);
        etLastnameR.setTypeface(quick_regular);
        etNameR.setTypeface(quick_regular);
        etPasswordR.setTypeface(quick_regular);
        etUsernameR.setTypeface(quick_regular);
        btSignUp.setTypeface(quick_bold);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLocale();
        if(Utils.TEMA == 0){
            setTheme(R.style.AppTheme);
        }else {
            setTheme(R.style.NightProfile);
        }
        setContentView(R.layout.activity_register);
        getSupportActionBar().hide();
        init();

        Glide
                .with(this)
                .load(R.drawable.output)
                .into(view6);

        btSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etLastnameR.getText().toString().equals("") || etUsernameR.getText().toString().equals("")
                        || etPasswordR.getText().toString().equals("") ||etNameR.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"You must fill all fields!",Toast.LENGTH_LONG).show();
                }else if(!etLastnameR.getText().toString().contains("@")){
                    Toast.makeText(getApplicationContext(),"Enter valid e-mail address",Toast.LENGTH_SHORT).show();
                }else {
                    registerUser();
                }
            }
        });

        tvGenerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etUsernameR.setText(usernameGenerator());
            }
        });


        etLastnameR.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(etLastnameR.getText().length()>0){
                    horizontalScrollView3.setVisibility(View.VISIBLE);
                }else{
                    horizontalScrollView3.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void registerUser() {

        StringRequest stringRequest3 = new StringRequest(Request.Method.POST, url_register, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.contains("zauzeto"))
                {
                    Toast.makeText(getApplicationContext(),"Username already exists!",Toast.LENGTH_SHORT).show();
                    tvGenerate.setVisibility(View.VISIBLE);

                }
                else if (response.contains("tacno"))
                {
                    Toast.makeText(getApplicationContext(),"Registration succesfull!",Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Register.this,Login.class);
                    startActivity(intent);
                    finish();
                }else {
                    Toast.makeText(Register.this,response,Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Something went wrong, try again...",Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("name",etNameR.getText().toString().trim());
                params.put("lastname",etLastnameR.getText().toString().trim());
                params.put("username",etUsernameR.getText().toString().trim());
                params.put("password",etPasswordR.getText().toString().trim());
                return params;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest3);
    }

    public String usernameGenerator() {
        Random random = new Random();
        int index = random.nextInt(proper_noun.length);
        int index2 = random.nextInt(proper_noun2.length);
        return (proper_noun[index]+proper_noun2[index2]);
    }


    public void concatMail(View view) {
        int id = view.getId();
        switch (id){
            case R.id.gmail:
                if(!etLastnameR.getText().toString().contains("@")){
                    String mail = etLastnameR.getText().toString();
                    etLastnameR.setText(mail.concat("@gmail.com"));
                }else {
                    String mail = etLastnameR.getText().toString();
                    String prefix = mail.split("@")[0];
                    etLastnameR.setText(prefix.concat("@gmail.com"));
                }
                break;
            case R.id.yahoo:
                if(!etLastnameR.getText().toString().contains("@")){
                    String mail = etLastnameR.getText().toString();
                    etLastnameR.setText(mail.concat("@yahoo.com"));
                }else {
                    String mail = etLastnameR.getText().toString();
                    String prefix = mail.split("@")[0];
                    etLastnameR.setText(prefix.concat("@yahoo.com"));
                }
                break;
            case R.id.hotmail:
                if(!etLastnameR.getText().toString().contains("@")){
                    String mail = etLastnameR.getText().toString();
                    etLastnameR.setText(mail.concat("@hotmail.com"));
                }else {
                    String mail = etLastnameR.getText().toString();
                    String prefix = mail.split("@")[0];
                    etLastnameR.setText(prefix.concat("@hotmail.com"));
                }
                break;
            case R.id.outlook:
                if(!etLastnameR.getText().toString().contains("@")){
                    String mail = etLastnameR.getText().toString();
                    etLastnameR.setText(mail.concat("@outlook.com"));
                }else {
                    String mail = etLastnameR.getText().toString();
                    String prefix = mail.split("@")[0];
                    etLastnameR.setText(prefix.concat("@outlook.com"));
                }
                break;
            case R.id.icloud:
                if(!etLastnameR.getText().toString().contains("@")){
                    String mail = etLastnameR.getText().toString();
                    etLastnameR.setText(mail.concat("@icloud.com"));
                }else {
                    String mail = etLastnameR.getText().toString();
                    String prefix = mail.split("@")[0];
                    etLastnameR.setText(prefix.concat("@icloud.com"));
                }
                break;
            case R.id.aol:
                if(!etLastnameR.getText().toString().contains("@")){
                    String mail = etLastnameR.getText().toString();
                    etLastnameR.setText(mail.concat("@aol.com"));
                }else {
                    String mail = etLastnameR.getText().toString();
                    String prefix = mail.split("@")[0];
                    etLastnameR.setText(prefix.concat("@aol.com"));
                }
                break;
            case R.id.yandex:
                if(!etLastnameR.getText().toString().contains("@")){
                    String mail = etLastnameR.getText().toString();
                    etLastnameR.setText(mail.concat("@yandex.com"));
                }else {
                    String mail = etLastnameR.getText().toString();
                    String prefix = mail.split("@")[0];
                    etLastnameR.setText(prefix.concat("@yandex.com"));
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if(!etLastnameR.getText().toString().equals("") || !etUsernameR.getText().toString().equals("")
                || !etPasswordR.getText().toString().equals("") ||!etNameR.getText().toString().equals("")){

            AlertDialog.Builder builder = new AlertDialog.Builder(Register.this);
            View view = getLayoutInflater().inflate(R.layout.alert_registration,null);

            ConstraintLayout alert_registration= view.findViewById(R.id.alert_registration);
            TextView tvPoruka = view.findViewById(R.id.tvPoruka);
            Button btClose = view.findViewById(R.id.button);
            Button btCancel = view.findViewById(R.id.btCancel);

            horizontalScrollView3 = view.findViewById(R.id.horizontalScrollView3);

            if(Utils.TEMA == 1){
                alert_registration.setBackground(getResources().getDrawable(R.drawable.gradient3_nigth_mode));

            }else {

                alert_registration.setBackground(getResources().getDrawable(R.drawable.gradient3));
            }

            btClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    finish();
                }
            });

            btCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });


            builder.setView(view);
            dialog = builder.create();
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }else {
            super.onBackPressed();
        }

    }

    public void loadLocale(){
        SharedPreferences preferences = getSharedPreferences("settings",MODE_PRIVATE);
        String language = preferences.getString("my_lang","");
        setLocale(language);
    }

    private void setLocale(String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.locale = locale;
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());

        SharedPreferences.Editor editor = getSharedPreferences("settings",MODE_PRIVATE).edit();
        editor.putString("my_lang",lang);
        editor.apply();
    }
}
