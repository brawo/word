package com.word.pc.word.Adapteri;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.tooltip.OnClickListener;
import com.tooltip.Tooltip;
import com.word.pc.word.Aktivnosti.Proile;
import com.word.pc.word.Helperi.MySingleton;
import com.word.pc.word.Misc.DbContract;
import com.word.pc.word.Misc.RangCalculator;
import com.word.pc.word.Modeli.Word;
import com.word.pc.word.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RecyclerAvatar extends RecyclerView.Adapter<RecyclerAvatar.MyViewHolder> {

    private ArrayList<String> data;
    private Context context;


    public RecyclerAvatar(ArrayList<String> data,Context context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerAvatar.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item_avatar,parent,false);
        return new RecyclerAvatar.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerAvatar.MyViewHolder holder, final int position) {


        holder.j = position+1;
        String pocetak = "avatars/";
        String kraj = data.get(position);
        String ext = ".png";
        String slika = pocetak.concat(kraj).concat(ext);


        try {
            holder.avatar = Drawable.createFromStream(holder.context.getAssets().open(slika), null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Glide
                .with(context)
                .load(holder.avatar)
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                .apply(RequestOptions.skipMemoryCacheOf(true))
                .apply(new RequestOptions().optionalCircleCrop())
                .into(holder.ivAvatar);

        holder.ivAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Glide
                        .with(context)
                        .load(holder.avatar)
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                        .apply(RequestOptions.skipMemoryCacheOf(true))
                        .apply(new RequestOptions().optionalCircleCrop())
                        .into(Proile.ivProfileP);
                //Toast.makeText(context,"klik na " + holder.j,Toast.LENGTH_SHORT).show();
                Proile.tvPrebacenAvatar.setText(String.valueOf(holder.j));

                if(Proile.prebacenAvatarInt!=holder.j) {
                    Proile.btChangeAvatar.setVisibility(View.VISIBLE);
                }else {
                    Proile.btChangeAvatar.setVisibility(View.INVISIBLE);
                }
            }
        });



    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {

        ImageView ivAvatar;
        Drawable avatar,avatar2;
        String pocetak = "avatars/";
        String kraj = data.get(0);
        String slika;
        Context context;
        Proile proile;
        int j;
        AlertDialog.Builder builder;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            slika = pocetak.concat(kraj);
            context = itemView.getContext();
            avatar = null;
            avatar2 = null;
            ivAvatar = itemView.findViewById(R.id.ivAvatar);
            j = 0;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}