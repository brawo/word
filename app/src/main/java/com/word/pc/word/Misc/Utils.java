package com.word.pc.word.Misc;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;

import com.word.pc.word.R;

import static android.content.Context.MODE_PRIVATE;

public class Utils
{
    public static int USER_ID = 0;
    public static int WRONG_PASSWORD = 0;
    private static int sTheme = 0;
    public final static int THEME_DEFAULT = 0;
    public final static int THEME_NIGHT = 1;

    public static int TEMA = 0;

    public static void changeToTheme(Activity activity, int theme)
    {
        sTheme = theme;
        activity.finish();
        activity.startActivity(new Intent(activity, activity.getClass()));
        activity.overridePendingTransition(android.R.anim.fade_in,
                android.R.anim.fade_out);

    }

    public static void onActivityCreateSetTheme(Activity activity)
    {
        switch (sTheme)
        {
            default:
            case THEME_DEFAULT:
                activity.setTheme(R.style.Day);
                break;
            case THEME_NIGHT:
                activity.setTheme(R.style.Night);
                break;

        }
    }
}