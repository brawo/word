package com.word.pc.word.Aktivnosti;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.word.pc.word.Helperi.DbHelper;
import com.word.pc.word.Helperi.MySingleton;
import com.word.pc.word.Misc.DbContract;
import com.word.pc.word.Misc.Utils;
import com.word.pc.word.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Pop extends AppCompatActivity {

    TextView tvPorukaUpdate, tvRecPrebaceno, tvPrevodPrebaceno;
    EditText etWordUpdate, ettranslateUpdate;
    Button btUpdate, btCancelUpdate;
    String recPrebaceno, prevodPrebaceno,whatToEdit,Word,Word2,Translate,Translate2;
    ConstraintLayout pop_layout;
    String url_updateWord = "http://lazyiguanastudios.com/webservices/Wordinary/updateWord.php";
    String url_updateTranslate = "http://lazyiguanastudios.com/webservices/Wordinary/updateTranslate.php";
    Typeface quick_bold, quick_light, quick_medium, quick_regular;

    public void init() {
        pop_layout = findViewById(R.id.pop_layout);
        tvPorukaUpdate = findViewById(R.id.tvPoruka);
        tvRecPrebaceno = findViewById(R.id.tvRecPrebaceno);
        tvPrevodPrebaceno = findViewById(R.id.tvPrevodPrebaceno);
        etWordUpdate = findViewById(R.id.etWordUpdate);
        ettranslateUpdate = findViewById(R.id.ettranslateUpdate);
        btUpdate = findViewById(R.id.btUpdate);
        btCancelUpdate = findViewById(R.id.btCancel);
        recPrebaceno = getIntent().getStringExtra("rec");
        prevodPrebaceno = getIntent().getStringExtra("prevod");
        whatToEdit = getIntent().getStringExtra("edit");
        tvRecPrebaceno.setText(recPrebaceno);
        tvPrevodPrebaceno.setText(prevodPrebaceno);

        quick_bold = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Bold.ttf");
        quick_light = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Light.ttf");
        quick_medium = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Medium.ttf");
        quick_regular = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Regular.ttf");

        tvPorukaUpdate.setTypeface(quick_regular);
        ettranslateUpdate.setTypeface(quick_regular);
        etWordUpdate.setTypeface(quick_regular);
        btCancelUpdate.setTypeface(quick_regular);
        btUpdate.setTypeface(quick_regular);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        getWindow().setLayout((int) (width * .8), (int) (height * .5));
        getSupportActionBar().hide();
        init();

        if (Utils.TEMA == 1) {
            pop_layout.setBackground(getResources().getDrawable(R.drawable.gradient3_nigth_mode));
        }else {
            pop_layout.setBackground(getResources().getDrawable(R.drawable.gradient3));
        }

        if(whatToEdit.equals("editWord")) {
            Word = tvRecPrebaceno.getText().toString();
            etWordUpdate.setText(Word);
            etWordUpdate.setEnabled(false);
            ettranslateUpdate.setHint(tvPrevodPrebaceno.getText().toString());
            ettranslateUpdate.setHintTextColor(getResources().getColor(R.color.providna_bela));
            ettranslateUpdate.requestFocus();
        }else {
            Translate2 = tvPrevodPrebaceno.getText().toString();
            ettranslateUpdate.setText(Translate2);
            ettranslateUpdate.setEnabled(false);
            etWordUpdate.setHint(tvRecPrebaceno.getText().toString());
            etWordUpdate.setHintTextColor(getResources().getColor(R.color.providna_bela));
            etWordUpdate.requestFocus();
        }

        btCancelUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(whatToEdit.equals("editWord")) {

                    if (checknetworkConncetion()) {
                        final DbHelper dbHelper = new DbHelper(getApplicationContext());
                        final SQLiteDatabase database = dbHelper.getWritableDatabase();


                        //Translate = ettranslateUpdate.getText().toString().trim();
                        if(ettranslateUpdate.getText().toString().trim().equals("")){
                            finish();
                            Toast.makeText(getApplicationContext(),"You haven't made any change!",Toast.LENGTH_LONG).show();
                        }else {
                            Translate = ettranslateUpdate.getText().toString().trim();
                        }

                        StringRequest stringRequest3 = new StringRequest(Request.Method.POST, url_updateTranslate, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {

                                    JSONObject jsonObject = new JSONObject(response);
                                    String Response = jsonObject.getString("response");
                                    if (Response.equals("OK")) {
                                        dbHelper.updateLocalDatabase(Word, Translate, Utils.USER_ID, DbContract.SYNC_STATUS_OK, database);
                                        finish();

                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getApplicationContext(),"Something went wrong, try again...",Toast.LENGTH_SHORT).show();

                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("word", Word);
                                params.put("translate", Translate);
                                params.put("user_id", String.valueOf(Utils.USER_ID));
                                return params;
                            }
                        };
                        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest3);


                    } else {

                        final DbHelper dbHelper = new DbHelper(getApplicationContext());
                        final SQLiteDatabase database = dbHelper.getWritableDatabase();

                        Word = tvRecPrebaceno.getText().toString();
                        Translate = ettranslateUpdate.getText().toString();

                        dbHelper.updateLocalDatabase(Word, Translate, Utils.USER_ID, DbContract.SYNC_STATUS_FAILED, database);
                        finish();

                    }

                }else {

                    if (checknetworkConncetion()) {
                        final DbHelper dbHelper = new DbHelper(getApplicationContext());
                        final SQLiteDatabase database = dbHelper.getWritableDatabase();

                        //Word2 = etWordUpdate.getText().toString();

                        if(etWordUpdate.getText().toString().trim().equals("")){
                            finish();
                            Toast.makeText(getApplicationContext(),"You haven't made any change!",Toast.LENGTH_LONG).show();
                        }else {
                            Word2 = etWordUpdate.getText().toString().trim();
                        }


                        StringRequest stringRequest3 = new StringRequest(Request.Method.POST, url_updateWord, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {

                                    JSONObject jsonObject = new JSONObject(response);
                                    String Response = jsonObject.getString("response");
                                    if (Response.equals("OK")) {
                                        updateLocalDatabaseByTranslate(Word2, Translate2, Utils.USER_ID, DbContract.SYNC_STATUS_OK, database);
                                        finish();

                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getApplicationContext(),"Something went wrong, try again...",Toast.LENGTH_SHORT).show();

                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("word", Word2);
                                params.put("translate", Translate2);
                                params.put("user_id", String.valueOf(Utils.USER_ID));
                                return params;
                            }
                        };
                        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest3);


                    } else {

                        final DbHelper dbHelper = new DbHelper(getApplicationContext());
                        final SQLiteDatabase database = dbHelper.getWritableDatabase();

                        Word2 = etWordUpdate.getText().toString();
                        Translate2 = tvPrevodPrebaceno.getText().toString();

                        updateLocalDatabaseByTranslate(Word2, Translate2, Utils.USER_ID, DbContract.SYNC_STATUS_FAILED, database);
                        finish();

                    }

                }

            }
        });
    }

    public boolean checknetworkConncetion() {
        ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public void updateLocalDatabaseByTranslate(String word,String translate,int user_id,int sync_status,SQLiteDatabase database)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DbContract.SYNC_STATUS,sync_status);
        contentValues.put(DbContract.WORD,word);
        contentValues.put(DbContract.USER_ID,user_id);
        String selection = DbContract.TRANSLATE+" = ?";
        String[] selection_args = {translate};
        database.update(DbContract.TABLE_NAME,contentValues,selection,selection_args);
    }
}
