package com.word.pc.word.Misc;

import android.app.Activity;

import com.word.pc.word.R;

public class RangCalculator {

    public static int THEME_VER = 0;

    public static String setRangName(int rangg) {
        String RankName = "";

        if (rangg >= 0 && rangg <= 5) {
            RankName = "Apprentice";
        } else if (rangg > 5 && rangg <= 15) {
            RankName = "Attendant";
        } else if (rangg > 15 && rangg <= 30) {
            RankName = "Secretary";
        } else if (rangg > 30 && rangg <= 50) {
            RankName = "Master Chief";
        } else if (rangg > 50 && rangg <= 70) {
            RankName = "Librarian";
        } else if (rangg > 70 && rangg <= 90) {
            RankName = "Vizier";
        } else if (rangg > 90 && rangg <= 120) {
            RankName = "Duke";
        } else if (rangg > 120 && rangg <= 150) {
            RankName = "High Shaman";
        } else if (rangg > 150 && rangg <= 200) {
            RankName = "Attendant";
        } else if (rangg > 200 && rangg <= 250) {
            RankName = "General";
        } else if (rangg > 250 && rangg <= 300) {
            RankName = "Grand Admiral";
        } else if (rangg > 300 && rangg <= 400) {
            RankName = "Royal Knight";
        } else if (rangg > 400 && rangg <= 550) {
            RankName = "Royal Councillor";
        } else if (rangg > 550 && rangg <= 700) {
            RankName = "Royal Strategos";
        } else if (rangg > 700 && rangg <= 900) {
            RankName = "Royal Spokesman";
        } else if (rangg > 900 && rangg <= 1000) {
            RankName = "Royal Advisor";
        } else if (rangg > 1000 && rangg <= 1300) {
            RankName = "Royal Executor";
        } else if (rangg > 1300 && rangg <= 1600) {
            RankName = "King's Advisor";
        } else if (rangg > 1600 && rangg <= 2000) {
            RankName = "Hand of the King";
        } else if (rangg > 2000 && rangg <= 3000) {
            RankName = "Prime Minister";
        } else if (rangg > 3000) {
            RankName = "Dragonlord";
        } else {
            RankName = "WOW!";
        }

        return RankName;
    }

    public static int setMaxScore(int rangg) {
        int maxScore = 0;

        if (rangg >= 0 && rangg <= 5) {
            maxScore = 5;
        } else if (rangg > 5 && rangg <= 15) {
            maxScore = 15;
        } else if (rangg > 15 && rangg <= 30) {
            maxScore = 30;
        } else if (rangg > 30 && rangg <= 50) {
            maxScore = 50;
        } else if (rangg > 50 && rangg <= 70) {
            maxScore = 70;
        } else if (rangg > 70 && rangg <= 90) {
            maxScore = 90;
        } else if (rangg > 90 && rangg <= 120) {
            maxScore = 120;
        } else if (rangg > 120 && rangg <= 150) {
            maxScore = 150;
        } else if (rangg > 150 && rangg <= 200) {
            maxScore = 200;
        } else if (rangg > 200 && rangg <= 250) {
            maxScore = 250;
        } else if (rangg > 250 && rangg <= 300) {
            maxScore = 300;
        } else if (rangg >= 300 && rangg <= 400) {
            maxScore = 400;
        } else if (rangg > 400 && rangg <= 550) {
            maxScore = 550;
        } else if (rangg > 550 && rangg <= 700) {
            maxScore = 700;
        } else if (rangg > 700 && rangg <= 900) {
            maxScore = 900;
        } else if (rangg > 900 && rangg <= 1000) {
            maxScore = 1000;
        } else if (rangg > 1000 && rangg <= 1300) {
            maxScore = 1300;
        } else if (rangg > 1300 && rangg <= 1600) {
            maxScore = 1600;
        } else if (rangg > 1600 && rangg <= 2000) {
            maxScore = 2000;
        } else if (rangg > 2000 && rangg <= 3000) {
            maxScore = 3000;
        } else if (rangg > 3000) {
            maxScore = 3000;
        }

        return maxScore;
    }

    public static String[] proper_noun = {"Angry", "Bored", "Creepy", "Hungry", "Jealous", "Lazy", "Arrogant", "Clumsy",
            "Adorable", "Beautiful", "Elegant", "Fancy", "Glamorous", "Rich", "Shy", "Dead",
            "Slow", "Old", "Loud", "Massive", "Tiny", "Great", "Chubby", "Scary",
            "Young", "Cool", "Filthy", "Fluffy", "Dirty"};
    public static String[] proper_noun2 = {"Baboon", "Bat", "Bear", "Beaver", "Bee", "Coyote", "Duck", "Ferret", "Iguana",
            "Cat", "Donkey", "Chicken", "Akita", "Armadillo", "Cougar", "Cow", "Fox", "Bison",
            "Goat", "Hyena", "Goose", "Horse", "Giraffe", "Buffalo", "Crocodile", "Hamster", "Chipmunk",
            "Lemur", "Peacock", "Pig", "Poodle", "Rat", "Seal", "Swan", "Wasp", "Puma"};


    public static String[] comments = {"I intend to live forever. So far, so good", "The first time I see a jogger smiling, I’ll consider it",
            "I’m addicted to placebos", "When nothing is going right, go left",
            "Reality continues to ruin my life", "Sane is boring", "I remixed a remix, it was back to normal",
            "You’re only as good as your last haircut", "What a nice night for an evening",
            "I drive way too fast to worry about cholesterol", "Work hard, nap hard",
            "Weather forecast for tonight: dark", "If you’re not having fun, you’re doing something wrong",
            "Behind every great man is a woman rolling her eyes", "I don’t want to be a vampire. I’m a day person",
            "I’m going to North Pole to help out Santa this year", "Insomnia is my greatest inspiration", "Mondays are fine. It’s my life that sucks",
            "Talking about music is like dancing about history", "A day without sunshine is like, you know, night",
            "Life does not imitate art, it imitates bad television", "Confidence is 10% work and 90% delusion",
            "I’m not a complete idiot. Some pieces are missing", "Have you ever wondered why you can’t taste your tongue?",
            "The person who laughs last at a joke...didn’t get it", "If you ever decide to leave me, I’m going with you",
            "Second is the first loser", "Out of my mind, back in five minutes", "Some mistakes are too much fun to make only once",
            "The secret of a happy marriage remains a secret", "What happens when you get 'scared half to death' twice?",
            "What if Batman gets bitten by a vampire?", "If I save time, when do I get it back?",
            "Do Lipton employees take coffee breaks?", "What is the protocol if the package says, \"Open somewhere else\"?",
            "If you're too open-minded; your brains will fall out", "My favorite machine at the gym is the vending machine",
            "I drink to make other people more interesting", "I’m in shape. Round is a shape", "Avoid fruits and nuts. You are what you eat",
            "I drank some boiling water because I wanted to whistle", "If you must make noise, make it quietly", "Man has his will, but woman has her way",
            "I can resist everything except temptation", "I intend to live forever. So far, so good"};


}
