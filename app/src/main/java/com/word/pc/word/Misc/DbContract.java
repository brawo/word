package com.word.pc.word.Misc;

public class DbContract {
    public static final int SYNC_STATUS_OK = 0;
    public static final int SYNC_STATUS_FAILED = 1;

    public static final String UI_UPDATE_BROADCAST = "com.example.sqlte_sql_test.uiupdatebroadcast";

    public static final String DATABASE_NAME = "wordinary";
    public static final String TABLE_NAME = "words";
    public static final String WORD = "word";
    public static final String TRANSLATE = "translate";
    public static final String USER_ID = "user_id";
    public static final String SYNC_STATUS = "syncstatus";
}
