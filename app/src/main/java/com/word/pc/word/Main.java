package com.word.pc.word;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewAnimationUtils;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.tooltip.Tooltip;
import com.word.pc.word.Adapteri.RecyclerAdapter;
import com.word.pc.word.Aktivnosti.LeaderBoard;
import com.word.pc.word.Aktivnosti.Proile;
import com.word.pc.word.Aktivnosti.Qiuz1;
import com.word.pc.word.Aktivnosti.Qiuz2;
import com.word.pc.word.Helperi.DbHelper;
import com.word.pc.word.Helperi.MySingleton;
import com.word.pc.word.Misc.DbContract;
import com.word.pc.word.Misc.RangCalculator;
import com.word.pc.word.Misc.Utils;
import com.word.pc.word.Modeli.Word;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.word.pc.word.Misc.RangCalculator.comments;


public class Main extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener {

    private static int sTheme = 0;
    public final static int THEME_DEFAULT = 0;
    public final static int THEME_NIGHT = 1;


    private boolean doubleBackToExitPressedOnce;
    private Handler mHandler = new Handler();
    RecyclerView rvWords;
    EditText etWord, etTranslate;
    Button btSubmit;
    RecyclerView.LayoutManager layoutManager;
    RecyclerAdapter adapter;
    ArrayList<Word> arrayList = new ArrayList<>();
    private ConstraintSet layout1, layout2;
    private ConstraintLayout constraintLayout;
    private boolean isOpen = false;
    String url_send = "http://lazyiguanastudios.com/webservices/Wordinary/connection.php";
    String url_allWords = "http://lazyiguanastudios.com/webservices/Wordinary/allWords.php";
    String url_details = "http://lazyiguanastudios.com/webservices/Wordinary/UserDetails2.php";
    String url_delete_wods = "http://lazyiguanastudios.com/webservices/Wordinary/deleteUserWords.php";
    FloatingActionButton fab, fab2;
    Switch nightMode;
    String word, translate, prebacenID, resultScore, comment;
    ImageView ivProfile, ivBadge;
    View view2;
    boolean isFliped = false;
    RelativeLayout relativeLayout;
    String resultIme, resultPrezime, resultRang, resultAvatar, resultHint, resultUsername, resultBrojReciNaServeru;
    int rang, score, brojReci, brojRecinaServeru, prebacenIDint, dialogInt;
    Context context;
    public static Drawable avatar, badge;
    TextView tvName, tvRang, tvOfflineWords;
    Animation animHorizontal;
    Typeface quick_bold, quick_light, quick_medium, quick_regular;
    boolean prebacenaTema = false;
    public static SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String theme = "";
    private String resultTranslate, resultWord;

    public void init() {

        preferences = getSharedPreferences("theme", MODE_PRIVATE);
        editor = preferences.edit();
        theme = preferences.getString("my_theme", "");

        rvWords = findViewById(R.id.rvWords);
        etWord = findViewById(R.id.etWord);
        etTranslate = findViewById(R.id.etTranslate);
        btSubmit = findViewById(R.id.btSubmit);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        layoutManager = new LinearLayoutManager(this);
        rvWords.setLayoutManager(layoutManager);
        rvWords.setHasFixedSize(true);
        //========================================================================================== rv optimizacija
        rvWords.setItemViewCacheSize(20);
        rvWords.setDrawingCacheEnabled(true);
        rvWords.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        //==========================================================================================
        adapter = new RecyclerAdapter(arrayList, this);
        adapter.setHasStableIds(true);
        rvWords.setAdapter(adapter);
        view2 = findViewById(R.id.view2);
        context = this;
        animHorizontal = AnimationUtils.loadAnimation(this, R.anim.shake_horizontal);
        prebacenID = getIntent().getStringExtra("id");
        //prebacenIDint = Integer.parseInt(prebacenID);

        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------ layout animacija

        layout1 = new ConstraintSet();
        layout2 = new ConstraintSet();
        constraintLayout = (ConstraintLayout) findViewById(R.id.constraint_layout);
        layout2.clone(this, R.layout.content_main_extended);
        layout1.clone(constraintLayout);

        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------ fontovi
        quick_bold = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Bold.ttf");
        quick_light = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Light.ttf");
        quick_medium = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Medium.ttf");
        quick_regular = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Regular.ttf");

        etTranslate.setTypeface(quick_regular);
        etWord.setTypeface(quick_regular);
        btSubmit.setTypeface(quick_medium);
        dialogInt = 0;

        rvWords.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {

                } else {

                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
                    fab.hide();
                } else if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    fab.hide();
                } else {
                    fab.show();
                }
            }
        });

        userDetails();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onActivityCreateSetTheme(this);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        init();
        readFromLocalStorage(Utils.USER_ID);

        if (Utils.TEMA == 1) {
            fab2.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.crna)));
            btSubmit.setBackground(getResources().getDrawable(R.drawable.button_back_upload_night));
            view2.setBackgroundColor(getResources().getColor(R.color.crna));

        } else {
            fab2.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
            btSubmit.setBackground(getResources().getDrawable(R.drawable.button_back));
            view2.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        }

        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------ fab/fab2 circular reveal

        fab.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View v) {
                if (!isOpen) {
                    TransitionManager.beginDelayedTransition(constraintLayout);
                    layout2.applyTo(constraintLayout);
                    isOpen = !isOpen;
                    fab2.animate().rotation(360f).setDuration(500);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        int cx = fab2.getWidth() / 2;
                        int cy = fab2.getHeight() / 2;

                        float finalRadius = (float) Math.hypot(cx, cy);

                        Animator anim =
                                ViewAnimationUtils.createCircularReveal(fab2, cx, cy, 0, finalRadius);

                        fab2.setVisibility(View.VISIBLE);

                        anim.start();
                        fab.setEnabled(false);


                    } else {
                        fab2.setVisibility(View.VISIBLE);
                        fab.setEnabled(false);
                    }


                } else {
                    TransitionManager.beginDelayedTransition(constraintLayout);
                    layout1.applyTo(constraintLayout);
                    isOpen = !isOpen;
                    fab2.animate().rotation(360f).setDuration(500);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        int cx = fab2.getWidth() / 2;
                        int cy = fab2.getHeight() / 2;

                        float finalRadius = (float) Math.hypot(cx, cy);

                        Animator anim =
                                ViewAnimationUtils.createCircularReveal(fab2, cx, cy, 0, finalRadius);

                        fab2.setVisibility(View.VISIBLE);
                        anim.start();
                        fab.setEnabled(false);
                    } else {
                        fab2.setVisibility(View.VISIBLE);
                        fab.setEnabled(false);
                    }
                }
            }
        });

        fab2.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View v) {
                if (!isOpen) {
                    TransitionManager.beginDelayedTransition(constraintLayout);
                    layout2.applyTo(constraintLayout);
                    isOpen = !isOpen;
                    fab2.animate().rotation(-360f).setDuration(500);
                    // Check if the runtime version is at least Lollipop
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        int cx = fab2.getWidth() / 2;
                        int cy = fab2.getHeight() / 2;

                        float initialRadius = (float) Math.hypot(cx, cy);

                        Animator anim =
                                ViewAnimationUtils.createCircularReveal(fab2, cx, cy, initialRadius, 0);

                        anim.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);

                                fab2.setVisibility(View.INVISIBLE);
                                fab.setEnabled(true);
                            }
                        });

                        anim.start();
                    } else {
                        fab2.setVisibility(View.INVISIBLE);
                        fab.setEnabled(true);
                    }


                } else {
                    TransitionManager.beginDelayedTransition(constraintLayout);
                    layout1.applyTo(constraintLayout);
                    isOpen = !isOpen;
                    fab2.animate().rotation(-360f).setDuration(500);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        int cx = fab2.getWidth() / 2;
                        int cy = fab2.getHeight() / 2;

                        float initialRadius = (float) Math.hypot(cx, cy);

                        Animator anim =
                                ViewAnimationUtils.createCircularReveal(fab2, cx, cy, initialRadius, 0);

                        anim.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                fab2.setVisibility(View.INVISIBLE);
                                fab.setEnabled(true);
                            }
                        });

                        anim.start();
                    } else {
                        // set the view to visible without a circular reveal animation below Lollipop
                        fab2.setVisibility(View.INVISIBLE);
                        fab.setEnabled(true);
                    }
                }
            }
        });

        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        tvOfflineWords = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().
                findItem(R.id.nav_send));
        getOfflineWords();
        nightMode = (Switch) MenuItemCompat.getActionView(navigationView.getMenu().
                findItem(R.id.nav_settings));


        if (theme.equals("")) {
            nightMode.setChecked(false);
        } else if (theme.equals("0")) {
            nightMode.setChecked(false);
        } else if (theme.equals("1")) {
            nightMode.setChecked(true);
        } else {
            nightMode.setChecked(false);
        }

        //------------------------------------------------------------------------------------------ todo switch da mena Utils.TEMA!!!!!!!

        nightMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (nightMode.isChecked()) {
                    changeToTheme(Main.this, 1);
                    Utils.TEMA = 1;
                    editor.putString("my_theme", "1");
                    editor.commit();

                } else {
                    changeToTheme(Main.this, 0);
                    prebacenaTema = false;
                    nightMode.setChecked(false);
                    Utils.TEMA = 0;
                    editor.putString("my_theme", "0");
                    editor.commit();
                }
            }
        });
        //------------------------------------------------------------------------------------------ promena teme

        View headerview = navigationView.getHeaderView(0);
        tvName = headerview.findViewById(R.id.tvName);
        tvRang = headerview.findViewById(R.id.tvRang);
        ivProfile = (ImageView) headerview.findViewById(R.id.ivProfile);
        ivBadge = (ImageView) headerview.findViewById(R.id.ivBadge);
        relativeLayout = headerview.findViewById(R.id.relativeLayoutProfile);

        if (Utils.TEMA == 1) {
            relativeLayout.setBackground(context.getResources().getDrawable(R.drawable.placeholder_night_mode));


        } else {
            relativeLayout.setBackground(context.getResources().getDrawable(R.drawable.placeholder));
        }

        tvName.setTypeface(quick_light);
        tvRang.setTypeface(quick_light);
        tvOfflineWords.setTypeface(quick_bold);
        comment = CommentGenerator();            //------------------------------------------------- jedan komentar da se generise po logovanju
        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tooltip tooltip = new Tooltip.Builder(ivProfile)
                        .setText(comment)
                        .setTextColor(Color.WHITE)
                        .setGravity(Gravity.BOTTOM)
                        .setCornerRadius(8f)
                        .setDismissOnClick(true)
                        .setCancelable(true)
                        .setBackgroundColor(getResources().getColor(R.color.zuta))
                        .show();
            }
        });


        //------------------------------------------------------------------------------------------ FLIP!

        tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ObjectAnimator oa1 = ObjectAnimator.ofFloat(relativeLayout, "scaleX", 1f, 0f);
                final ObjectAnimator oa2 = ObjectAnimator.ofFloat(relativeLayout, "scaleX", 0f, 1f);
                oa1.setInterpolator(new DecelerateInterpolator());
                oa2.setInterpolator(new AccelerateDecelerateInterpolator());
                oa1.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);

                        if (isFliped == false) {
                            relativeLayout.setBackground(getResources().getDrawable(R.drawable.placeholder_yelow));
                            ivProfile.setVisibility(View.INVISIBLE);
                            tvRang.setVisibility(View.VISIBLE);
                            oa2.start();
                            isFliped = true;
                        } else {
                            relativeLayout.setBackground(getResources().getDrawable(R.drawable.placeholder));
                            ivProfile.setVisibility(View.VISIBLE);
                            tvRang.setVisibility(View.INVISIBLE);
                            oa2.start();
                            isFliped = false;
                        }
                    }
                });
                oa1.start();
            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (doubleBackToExitPressedOnce) {
            finish();

        } else if (!drawer.isDrawerOpen(GravityCompat.START)) {

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            mHandler.postDelayed(mRunnable, 2000);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        //------------------------------------------------------------------------------------------ search bar
        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            Intent intent = new Intent(getApplicationContext(), Proile.class);
            intent.putExtra("name", resultIme);
            intent.putExtra("lastname", resultPrezime);
            intent.putExtra("rang", resultRang);
            intent.putExtra("score", resultScore);
            intent.putExtra("avatar", resultAvatar);
            startActivity(intent);

        } else if (id == R.id.nav_gallery) {
            if (brojRecinaServeru >= 50) {
                Intent intent = new Intent(getApplicationContext(), Qiuz1.class);
                intent.putExtra("name", resultIme);
                intent.putExtra("lastname", resultPrezime);
                intent.putExtra("rang", resultRang);
                intent.putExtra("score", resultScore);
                intent.putExtra("hint", resultHint);
                intent.putExtra("avatar", resultAvatar);
                intent.putExtra("id", prebacenID);
                startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), "You must have at least 50 words!", Toast.LENGTH_LONG).show();

            }

        } else if (id == R.id.nav_slideshow) {
            if (brojRecinaServeru >= 250) {
                Intent intent = new Intent(getApplicationContext(), Qiuz2.class);
                intent.putExtra("name", resultIme);
                intent.putExtra("lastname", resultPrezime);
                intent.putExtra("rang", resultRang);
                intent.putExtra("score", resultScore);
                intent.putExtra("hint", resultHint);
                intent.putExtra("avatar", resultAvatar);
                intent.putExtra("id", prebacenID);
                startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), "You must have at least 250 words!", Toast.LENGTH_SHORT).show();
            }


        } else if (id == R.id.nav_share) {
            if (score > 16) {
                Intent intent = new Intent(getApplicationContext(), LeaderBoard.class);
                intent.putExtra("username", resultUsername);
                intent.putExtra("name", resultIme);
                intent.putExtra("lastname", resultPrezime);
                intent.putExtra("rang", resultRang);
                intent.putExtra("score", resultScore);
                intent.putExtra("hint", resultHint);
                intent.putExtra("avatar", resultAvatar);
                intent.putExtra("id", prebacenID);
                startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), "You must be at least Attendant!", Toast.LENGTH_SHORT).show();
            }

        } else if (id == R.id.nav_send) {
            if (checknetworkConncetion()) {
                word = etWord.getText().toString();
                translate = etTranslate.getText().toString();
                final DbHelper dbHelper = new DbHelper(getApplicationContext());
                final SQLiteDatabase database = dbHelper.getWritableDatabase();

                Cursor cursor = database.rawQuery("SELECT * FROM words WHERE user_id =" + Utils.USER_ID, null);//selekcija reci po user_id

                while (cursor.moveToNext()) {
                    int sync_status = cursor.getInt(cursor.getColumnIndex(DbContract.SYNC_STATUS));
                    if (sync_status == DbContract.SYNC_STATUS_FAILED) {
                        final String Word = cursor.getString(cursor.getColumnIndex(DbContract.WORD));
                        final String Translate = cursor.getString(cursor.getColumnIndex(DbContract.TRANSLATE));

                        StringRequest stringRequest3 = new StringRequest(Request.Method.POST, url_send, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {

                                    JSONObject jsonObject = new JSONObject(response);
                                    String Response = jsonObject.getString("response");
                                    if (Response.equals("OK")) {
                                        dbHelper.updateLocalDatabase(Word, Translate, Utils.USER_ID, DbContract.SYNC_STATUS_OK, database);
                                        readFromLocalStorage(Utils.USER_ID);
                                        getOfflineWords();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                getOfflineWords();

                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("word", Word);
                                params.put("translate", Translate);
                                params.put("user_id", String.valueOf(Utils.USER_ID));
                                return params;
                            }
                        };
                        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest3);
                    }
                }

            }
        } else if (id == R.id.nav_settings) {


        } else if (id == R.id.nav_exit) {
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //----------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------

    private void saveToAppServer(final String Word, final String Translate) {

        if (checknetworkConncetion()) {
            StringRequest stringRequest3 = new StringRequest(Request.Method.POST, url_send, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String Response = jsonObject.getString("response");
                        if (Response.equals("OK")) {
                            saveToLocalStorage(Word, Translate, Utils.USER_ID, DbContract.SYNC_STATUS_OK);
                            getOfflineWords();

                        } else {
                            saveToLocalStorage(Word, Translate, Utils.USER_ID, DbContract.SYNC_STATUS_FAILED);
                            getOfflineWords();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    saveToLocalStorage(Word, Translate, Utils.USER_ID, DbContract.SYNC_STATUS_FAILED);
                    getOfflineWords();

                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("word", Word);
                    params.put("translate", Translate);
                    params.put("user_id", String.valueOf(Utils.USER_ID));
                    return params;
                }
            };
            MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest3);

        } else {
            saveToLocalStorage(word, translate, Utils.USER_ID, DbContract.SYNC_STATUS_FAILED);
            getOfflineWords();
        }


    }

    //----------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------- cuvanje u SQLite

    private void saveToLocalStorage(String word, String translate, int user_id, int sync_status) {
        DbHelper dbHelper = new DbHelper(this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        dbHelper.SaveToLocalDatabase(word, translate, user_id, sync_status, database);
        readFromLocalStorage(Utils.USER_ID);
        adapter.notifyDataSetChanged();

    }

    //----------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------- slanje reci

    public void submitWord(View view) {


        if (etWord.getText().toString().equals("") || etTranslate.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "You must fill all fields!", Toast.LENGTH_SHORT).show();
            //btSubmit.startAnimation(animHorizontal);
        } else {
            word = etWord.getText().toString();
            translate = etTranslate.getText().toString();
            saveToAppServer(word, translate);
            //saveToLocalStorage(word, translate, 1, 0);

            //---------------------------------------------------------------------------------------------------------------

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                int cx = view2.getWidth() / 2;
                int cy = view2.getHeight() / 2;

                float finalRadius = (float) Math.hypot(cx, cy);

                Animator anim =
                        ViewAnimationUtils.createCircularReveal(view2, cx, cy, 0, finalRadius);

                view2.setVisibility(View.VISIBLE);
                anim.start();
                anim.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            int cx = view2.getWidth() / 2;
                            int cy = view2.getHeight() / 2;

                            float initialRadius = (float) Math.hypot(cx, cy);

                            Animator anim =
                                    ViewAnimationUtils.createCircularReveal(view2, cx, cy, initialRadius, 0);

                            anim.addListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    view2.setVisibility(View.INVISIBLE);

                                }
                            });

                            anim.start();
                        } else {
                            view2.setVisibility(View.INVISIBLE);
                        }


                        super.onAnimationEnd(animation);
                    }
                });

            }

            //---------------------------------------------------------------------------------------------------------------

            etWord.setText("");
            etTranslate.setText("");
        }

    }

    //----------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------- citanje iz SQLite
    private void readFromLocalStorage(int userID) {
        Utils.USER_ID = userID;
        arrayList.clear();
        DbHelper dbHelper = new DbHelper(this);
        SQLiteDatabase database = dbHelper.getReadableDatabase();
        //Cursor cursor = dbHelper.readFromLocalDatbase(database);
        Cursor cursor = database.rawQuery("SELECT * FROM words WHERE user_id =" + userID + " ORDER BY word ASC", null);//selekcija reci po user_id

        while (cursor.moveToNext()) {
            String name = cursor.getString(cursor.getColumnIndex(DbContract.WORD));
            String translate = cursor.getString(cursor.getColumnIndex(DbContract.TRANSLATE));
            int sync_status = cursor.getInt(cursor.getColumnIndex(DbContract.SYNC_STATUS));
            int user_id = cursor.getInt(cursor.getColumnIndex(DbContract.USER_ID));
            arrayList.add(new Word(name, translate, sync_status, user_id));
        }

        adapter.notifyDataSetChanged();
        //cursor.close();

    }

    private void DeleteAllWordsFromServer() {
        StringRequest stringRequest2 = new StringRequest(Request.Method.POST, url_delete_wods, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(getApplicationContext(), "All set! Let's create new Wordinary", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Something went wrong, try again...",Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", String.valueOf(Utils.USER_ID));
                return params;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest2);

    }

    private void getAllWordsFromServer() {
        StringRequest stringRequest2 = new StringRequest(Request.Method.POST, url_allWords, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.toString());

                    JSONArray jsonArray = jsonObject.getJSONArray("result");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonRow = jsonArray.getJSONObject(i);
                        resultWord = jsonRow.getString("word");
                        resultTranslate = jsonRow.getString("translate");

                        saveToLocalStorage(resultWord, resultTranslate, Utils.USER_ID, DbContract.SYNC_STATUS_OK);
                        //readFromLocalStorage(Utils.USER_ID);
                    }
                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Something went wrong, try again...",Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", String.valueOf(Utils.USER_ID));
                return params;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest2);


    }

    public void updateMethod(String word, String translate, int userID) {
        Utils.USER_ID = userID;
        DbHelper dbHelper = new DbHelper(this);
        SQLiteDatabase database = dbHelper.getReadableDatabase();
        String query = "update words set word = ? , translate = ? where user_id = ?";
        String[] selections = {word, translate, String.valueOf(userID)};
        Cursor cursor = database.rawQuery(query, selections);
        cursor.close();
    }

    //----------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------- provera internet konekcije
    public boolean checknetworkConncetion() {
        ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    //----------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------- search bar

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        newText = newText.toLowerCase();
        ArrayList<Word> newList = new ArrayList<>();
        for (Word word : arrayList) {
            String Word = word.getWord().toString();
            String Translate = word.getTranslate().toString();
            if (Word.contains(newText)) {
                newList.add(word);
            } else if (Translate.contains(newText)) {
                newList.add(word);
            }

        }
        adapter.setFilter(newList);
        return true;
    }
    //----------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------

    private void userDetails() {

        StringRequest stringRequest2 = new StringRequest(Request.Method.POST, url_details, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jsonObject = new JSONObject(response.toString());

                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    JSONObject jsonRow = jsonArray.getJSONObject(0);

                    resultUsername = jsonRow.getString("username");
                    resultIme = jsonRow.getString("name");
                    resultPrezime = jsonRow.getString("lastname");
                    resultRang = jsonRow.getString("rang");
                    rang = Integer.parseInt(resultRang);
                    resultScore = jsonRow.getString("score");
                    score = Integer.parseInt(resultScore);
                    resultAvatar = jsonRow.getString("avatar");
                    resultHint = jsonRow.getString("hint");
                    resultBrojReciNaServeru = jsonRow.getString("brojReci");
                    brojRecinaServeru = Integer.parseInt(resultBrojReciNaServeru);


                    String pocetakA = "avatars/";
                    String pocetakB = "badges/";
                    String slika = pocetakA.concat(resultAvatar).concat(".png");
                    String slikaBadge = pocetakB.concat(resultRang).concat(".png");

                    try {
                        avatar = Drawable.createFromStream(context.getAssets().open(slika), null);
                        badge = Drawable.createFromStream(context.getAssets().open(slikaBadge), null);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    Glide
                            .with(context)
                            .load(avatar)
                            .apply(new RequestOptions().optionalCircleCrop())
                            .apply(new RequestOptions().placeholder(R.drawable.user2))
                            .into(ivProfile);

                    Glide
                            .with(context)
                            .load(badge)
                            .apply(new RequestOptions().placeholder(R.drawable.medal))
                            .into(ivBadge);

                    tvName.setText(resultIme);
                    tvRang.setText(RangCalculator.setRangName(score));


                } catch (JSONException e) {

                }


                if (adapter.getItemCount() == 0 && arrayList.isEmpty() && !resultBrojReciNaServeru.equals("0")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    View view = getLayoutInflater().inflate(R.layout.alert_get_words_from_server, null);

                    ConstraintLayout alert_layout = view.findViewById(R.id.alert_layout_from_server);
                    TextView tvPoruka = view.findViewById(R.id.tvPoruka);
                    Button btDownloadFromServer = view.findViewById(R.id.buttonFromServer);
                    Button btCancel = view.findViewById(R.id.btCancel);
                    ImageView ivUser = view.findViewById(R.id.imageView4);


                    if (Utils.TEMA == 1) {
                        alert_layout.setBackground(context.getResources().getDrawable(R.drawable.gradient3_nigth_mode));

                    } else {
                        alert_layout.setBackground(context.getResources().getDrawable(R.drawable.gradient3));
                    }


                    builder.setView(view);
                    final AlertDialog dialog = builder.create();

                    dialog.setCanceledOnTouchOutside(false);
                    if (dialogInt == 0) {
                        dialog.show();
                        dialogInt++;
                    }

                    btDownloadFromServer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getAllWordsFromServer();
                            dialog.dismiss();
                        }
                    });

                    btCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DeleteAllWordsFromServer();
                            dialog.dismiss();
                        }
                    });

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Something went wrong, try again...",Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("id", String.valueOf(Utils.USER_ID));
                return params;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest2);

    }

    public void getOfflineWords() {
        DbHelper dbHelper = new DbHelper(this);
        SQLiteDatabase database = dbHelper.getReadableDatabase();
        //brojReci = dbHelper.readWordNotOnServer(database);//------------------------------------------- broj reci koje nisu upload-ovane
        //------------------------------------------------------------------------------------------
        int nombr = 0;
        Cursor cursor = database.rawQuery("SELECT word FROM words WHERE syncstatus = 1 AND user_id =" + Utils.USER_ID, null);
        brojReci = cursor.getCount();
        //------------------------------------------------------------------------------------------
        if (brojReci > 0 && brojReci <= 10) {
            tvOfflineWords.setVisibility(View.VISIBLE);
            tvOfflineWords.setGravity(Gravity.CENTER_VERTICAL);
            tvOfflineWords.setTypeface(null, Typeface.BOLD);
            tvOfflineWords.setTextColor(getResources().getColor(R.color.colorAccent));
            tvOfflineWords.setText(String.valueOf(brojReci));
        } else if (brojReci > 10) {
            tvOfflineWords.setVisibility(View.VISIBLE);
            tvOfflineWords.setGravity(Gravity.CENTER_VERTICAL);
            tvOfflineWords.setTypeface(null, Typeface.BOLD);
            tvOfflineWords.setTextColor(getResources().getColor(R.color.colorAccent));
            tvOfflineWords.setText("10+");
        } else {
            tvOfflineWords.setVisibility(View.INVISIBLE);
        }
    }

    private final Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            doubleBackToExitPressedOnce = false;
        }
    };

    @Override
    protected void onDestroy() {


        if (mHandler != null) {
            mHandler.removeCallbacks(mRunnable);
        }

        nightMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (nightMode.isChecked()) {
                    changeToTheme(Main.this, 1);
                    Utils.TEMA = 1;

                } else {
                    changeToTheme(Main.this, 0);
                    prebacenaTema = false;
                    nightMode.setChecked(false);
                    Utils.TEMA = 0;
                }
            }
        });

        super.onDestroy();
    }


    @Override
    protected void onResume() {
        readFromLocalStorage(Utils.USER_ID);
        getOfflineWords();
        userDetails();
        if (Utils.TEMA == 1) {
            setTheme(R.style.Night);
            nightMode.setChecked(true);
        } else {
            setTheme(R.style.Day);
            nightMode.setChecked(false);
        }
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    public void changeToTheme(Activity activity, int theme) {
        sTheme = theme;
        activity.finish();
        activity.startActivity(new Intent(activity, activity.getClass()));
        activity.overridePendingTransition(android.R.anim.fade_in,
                android.R.anim.fade_out);

        SharedPreferences.Editor editor = getSharedPreferences("theme", MODE_PRIVATE).edit();
        editor.putString("my_theme", String.valueOf(theme));
        editor.apply();

    }

    public void onActivityCreateSetTheme(Activity activity) {
        if (!theme.equals("")) {
            sTheme = Integer.parseInt(theme);

            switch (sTheme) {
                case THEME_DEFAULT:
                    activity.setTheme(R.style.Day);
                    Utils.TEMA = 0;
                    break;
                case THEME_NIGHT:
                    activity.setTheme(R.style.Night);
                    Utils.TEMA = 1;
                    break;

            }

        } else {

            switch (sTheme) {
                case THEME_DEFAULT:
                    activity.setTheme(R.style.Day);
                    Utils.TEMA = 0;
                    break;
                case THEME_NIGHT:
                    activity.setTheme(R.style.Night);
                    Utils.TEMA = 1;
                    break;

            }
        }
    }

    public String CommentGenerator() {
        Random random = new Random();
        int index = random.nextInt(comments.length);
        return (comments[index]);
    }

}
