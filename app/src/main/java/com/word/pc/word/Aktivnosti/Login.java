package com.word.pc.word.Aktivnosti;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.word.pc.word.Helperi.MySingleton;
import com.word.pc.word.Main;
import com.word.pc.word.Misc.Utils;
import com.word.pc.word.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Login extends AppCompatActivity {

    private static int sTheme = 0;
    public final static int THEME_DEFAULT = 0;
    public final static int THEME_NIGHT = 1;
    SharedPreferences preferences;
    String theme;

    ConstraintLayout login_layout;
    TextView tvNaslovL, tvObavestenje1, tvObavestenje2, rezultat;
    ImageView globe;
    ImageView view6;
    EditText etUsernameL, etPasswordL,etNameReset,etNameEmail,etNewPassword;
    CheckBox checkBox;
    Button btLogin;
    //String url_login = "http://lazyiguanastudios.com/webservices/Wordinary/login.php";
    String url_login = "http://lazyiguanastudios.com/webservices/Wordinary/login2.php";
    String url_changePassword = "http://lazyiguanastudios.com/webservices/Wordinary/passwordReset.php";
    String [] itemList = {"English","Russian"};
    Typeface quick_bold, quick_light, quick_medium, quick_regular;
    AlertDialog dialog;
    HorizontalScrollView horizontalScrollView3;

    //---------------------------------------------------------------------------------------------- cuvanje cred
    String username, password, id;
    SharedPreferences loginPreferences;
    SharedPreferences.Editor loginPrefsEditor;
    Boolean saveLogin;
    //----------------------------------------------------------------------------------------------

    public void init() {

        Main.preferences = getSharedPreferences("theme",MODE_PRIVATE);
        theme = Main.preferences.getString("my_theme","");

        view6 = findViewById(R.id.view6);
        tvNaslovL = findViewById(R.id.tvNaslovL);
        tvObavestenje1 = findViewById(R.id.tvObavestenje1);
        tvObavestenje2 = findViewById(R.id.tvObavestenje2);
        etUsernameL = findViewById(R.id.etUsernameL);
        etPasswordL = findViewById(R.id.etPasswordL);
        btLogin = findViewById(R.id.btLogin);
        rezultat = findViewById(R.id.rezultat);
        login_layout = findViewById(R.id.login_layout);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        globe = findViewById(R.id.globe);

        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------ fontovi
        quick_bold = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Bold.ttf");
        quick_light = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Light.ttf");
        quick_medium = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Medium.ttf");
        quick_regular = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Regular.ttf");

        tvNaslovL.setTypeface(quick_light);
        tvObavestenje1.setTypeface(quick_regular);
        tvObavestenje2.setTypeface(quick_bold);
        etUsernameL.setTypeface(quick_regular);
        etPasswordL.setTypeface(quick_regular);
        btLogin.setTypeface(quick_bold);
        checkBox.setTypeface(quick_regular);

        if(Utils.TEMA == 1){
            login_layout.setBackgroundColor(getResources().getColor(R.color.crna));
            btLogin.setBackground(getResources().getDrawable(R.drawable.button_back_night_mode));
        }else {
            login_layout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            btLogin.setBackground(getResources().getDrawable(R.drawable.button_back_white_white_stroke));
        }

        //------------------------------------------------------------------------------------------ cuvanje user/pass

        loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        loginPrefsEditor = loginPreferences.edit();

        saveLogin = loginPreferences.getBoolean("saveLogin", false);
        if (saveLogin == true) {
            etUsernameL.setText(loginPreferences.getString("username", ""));
            //etUsername.setText(loginPreferences.getString(getString(R.string.username), ""));
            etPasswordL.setText(loginPreferences.getString("password", ""));
            //etPassword.setText(loginPreferences.getString(getString(R.string.password), ""));
            rezultat.setText(loginPreferences.getString("id", ""));
            //rezultat.setText(loginPreferences.getString(getString(R.string.id), ""));
            checkBox.setChecked(true);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLocale();
        if(Utils.TEMA == 0){
            setTheme(R.style.AppTheme);
        }else {
            setTheme(R.style.NightProfile);
        }
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        init();

        Glide
                .with(this)
                .load(R.drawable.output)
                .into(view6);

        tvObavestenje2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Register.class);
                startActivity(intent);
            }
        });

        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = etUsernameL.getText().toString();
                password = etPasswordL.getText().toString();
                id = rezultat.getText().toString();

                if(etUsernameL.getText().toString().equals("")||etPasswordL.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"Username and password are required!",Toast.LENGTH_SHORT).show();
                }
                else if (checkBox.isChecked()) {
                    loginPrefsEditor.putBoolean("saveLogin", true);
                    loginPrefsEditor.putString("username", username);
                    //loginPrefsEditor.putString(getString(R.string.username), username);
                    loginPrefsEditor.putString("password", password);
                    //loginPrefsEditor.putString(getString(R.string.password), password);
                    loginPrefsEditor.putString("id", id);
                    //loginPrefsEditor.putString(getString(R.string.id), id);
                    loginPrefsEditor.commit();

                    LoginUser();

                } else {
                    loginPrefsEditor.clear();
                    loginPrefsEditor.commit();

                    LoginUser();

                }
            }
        });

        //------------------------------------------------------------------------------------------ TODO OVO RADI! ODRADI SVE JEZIKE!!!
        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------ todo TEST ZA PROMENU JEZIKA!!!!!

        globe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeLang();
            }
        });
    }

    private void changeLang() {
        final String [] itemList = {"English","Russian","German","French"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("choose..");
        builder.setSingleChoiceItems(itemList, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(which==0){
                    setLocale("en");
                    recreate();

                }else if(which==1){
                    setLocale("ru");
                    recreate();
                }else if(which==2){
                    setLocale("de");
                    recreate();

                }else if(which==3){
                    setLocale("fr");
                    recreate();
                }
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setLocale(String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.locale = locale;
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());

        SharedPreferences.Editor editor = getSharedPreferences("settings",MODE_PRIVATE).edit();
        editor.putString("my_lang",lang);
        editor.apply();
    }

    public void loadLocale(){
        SharedPreferences preferences = getSharedPreferences("settings",MODE_PRIVATE);
        String language = preferences.getString("my_lang","");
        setLocale(language);
    }

        //------------------------------------------------------------------------------------------ todo TEST ZA PROMENU JEZIKA!!!!!
        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------ TODO OVO RADI! ODRADI SVE JEZIKE!!!

    public void LoginUser() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url_login, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.contains("netacn")) {
                    Utils.WRONG_PASSWORD++;
                    Snackbar snackbar = Snackbar.make(login_layout, "Wrong username or password!", Snackbar.LENGTH_SHORT);
                    View snkview = snackbar.getView();
                    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) snkview.getLayoutParams();
                    params.gravity = Gravity.BOTTOM;
                    snkview.setLayoutParams(params);
                    snkview.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.prljava));
                    snackbar.show();

                    if(Utils.WRONG_PASSWORD>=2){
                        AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
                        View view = getLayoutInflater().inflate(R.layout.alert_resset_password,null);

                        ConstraintLayout alert_reset_layout= view.findViewById(R.id.alert_reset_layout);
                        TextView tvPoruka = view.findViewById(R.id.tvPoruka);
                        Button btChangePassword = view.findViewById(R.id.button);
                        Button btCancel = view.findViewById(R.id.btCancel);
                        ImageView ivUser = view.findViewById(R.id.imageView4);
                        etNameReset= view.findViewById(R.id.etWordUpdate);
                        etNameEmail= view.findViewById(R.id.ettranslateUpdate);
                        etNewPassword= view.findViewById(R.id.etNewPassword);
                        horizontalScrollView3 = view.findViewById(R.id.horizontalScrollView3);

                        if(Utils.TEMA == 1){
                            alert_reset_layout.setBackground(getResources().getDrawable(R.drawable.gradient3_nigth_mode));

                        }else {

                            alert_reset_layout.setBackground(getResources().getDrawable(R.drawable.gradient3));
                        }

                        builder.setView(view);
                        dialog = builder.create();
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.show();


                        etNameEmail.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                if(etNameEmail.getText().length()>0){
                                    horizontalScrollView3.setVisibility(View.VISIBLE);
                                }else{
                                    horizontalScrollView3.setVisibility(View.INVISIBLE);
                                }
                            }

                            @Override
                            public void afterTextChanged(Editable s) {

                            }
                        });

                        btChangePassword.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(etNameReset.getText().toString().equals("")||etNameEmail.getText().toString().equals("")||etNewPassword.getText().toString().equals("")){
                                    Toast.makeText(getApplicationContext(),"You must fill all fields!",Toast.LENGTH_SHORT).show();
                                }else {
                                    changePassword();

                                }
                            }
                        });

                        btCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.cancel();
                            }
                        });
                    }

                } else {

                    try {

                        JSONObject jsonObject = new JSONObject(response.toString());

                        JSONArray jsonArray = jsonObject.getJSONArray("result");
                        JSONObject jsonRow = jsonArray.getJSONObject(0);

                        String resultStr = jsonRow.getString("id");
                        rezultat.setText(resultStr);
                        Utils.USER_ID = Integer.parseInt(resultStr);
                        Utils.WRONG_PASSWORD = 0;
                        Intent intent = new Intent(Login.this, Main.class);
                        intent.putExtra("id",resultStr);
                        startActivity(intent);
                        finish();
                    } catch (JSONException e) {

                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Snackbar snackbar = Snackbar.make(login_layout, "Something went wrong!", Snackbar.LENGTH_SHORT);
                View snkview = snackbar.getView();
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) snkview.getLayoutParams();
                params.gravity = Gravity.TOP;
                snkview.setLayoutParams(params);
                snkview.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.prljava));
                snackbar.show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", etUsernameL.getText().toString().trim());
                params.put("password", etPasswordL.getText().toString().trim());
                return params;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    private void changePassword() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url_changePassword, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.contains("nema")){
                    Toast.makeText(getApplicationContext(),"Wrong full name or email",Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getApplicationContext(),"Password successfully changed!",Toast.LENGTH_SHORT).show();
                    Utils.WRONG_PASSWORD=0;
                    dialog.cancel();
                    etPasswordL.setText("");
                    checkBox.setChecked(false);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Snackbar snackbar = Snackbar.make(login_layout, "Something went wrong, try again...", Snackbar.LENGTH_SHORT);
                View snkview = snackbar.getView();
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) snkview.getLayoutParams();
                params.gravity = Gravity.TOP;
                snkview.setLayoutParams(params);
                snkview.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.prljava));
                snackbar.show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("password", etNewPassword.getText().toString());
                params.put("name", etNameReset.getText().toString());
                params.put("lastname", etNameEmail.getText().toString());
                return params;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    public void concatMail(View view) {
        int id = view.getId();
        switch (id){
            case R.id.gmail:
                if(!etNameEmail.getText().toString().contains("@")){
                    String mail = etNameEmail.getText().toString();
                    etNameEmail.setText(mail.concat("@gmail.com"));
                }else {
                    String mail = etNameEmail.getText().toString();
                    String prefix = mail.split("@")[0];
                    etNameEmail.setText(prefix.concat("@gmail.com"));
                }
                break;
            case R.id.yahoo:
                if(!etNameEmail.getText().toString().contains("@")){
                    String mail = etNameEmail.getText().toString();
                    etNameEmail.setText(mail.concat("@yahoo.com"));
                }else {
                    String mail = etNameEmail.getText().toString();
                    String prefix = mail.split("@")[0];
                    etNameEmail.setText(prefix.concat("@yahoo.com"));
                }
                break;
            case R.id.hotmail:
                if(!etNameEmail.getText().toString().contains("@")){
                    String mail = etNameEmail.getText().toString();
                    etNameEmail.setText(mail.concat("@hotmail.com"));
                }else {
                    String mail = etNameEmail.getText().toString();
                    String prefix = mail.split("@")[0];
                    etNameEmail.setText(prefix.concat("@hotmail.com"));
                }
                break;
            case R.id.outlook:
                if(!etNameEmail.getText().toString().contains("@")){
                    String mail = etNameEmail.getText().toString();
                    etNameEmail.setText(mail.concat("@outlook.com"));
                }else {
                    String mail = etNameEmail.getText().toString();
                    String prefix = mail.split("@")[0];
                    etNameEmail.setText(prefix.concat("@outlook.com"));
                }
                break;
            case R.id.icloud:
                if(!etNameEmail.getText().toString().contains("@")){
                    String mail = etNameEmail.getText().toString();
                    etNameEmail.setText(mail.concat("@icloud.com"));
                }else {
                    String mail = etNameEmail.getText().toString();
                    String prefix = mail.split("@")[0];
                    etNameEmail.setText(prefix.concat("@icloud.com"));
                }
                break;
            case R.id.aol:
                if(!etNameEmail.getText().toString().contains("@")){
                    String mail = etNameEmail.getText().toString();
                    etNameEmail.setText(mail.concat("@aol.com"));
                }else {
                    String mail = etNameEmail.getText().toString();
                    String prefix = mail.split("@")[0];
                    etNameEmail.setText(prefix.concat("@aol.com"));
                }
                break;
            case R.id.yandex:
                if(!etNameEmail.getText().toString().contains("@")){
                    String mail = etNameEmail.getText().toString();
                    etNameEmail.setText(mail.concat("@yandex.com"));
                }else {
                    String mail = etNameEmail.getText().toString();
                    String prefix = mail.split("@")[0];
                    etNameEmail.setText(prefix.concat("@yandex.com"));
                }
                break;
        }
    }

    public void changeToTheme(Activity activity, int theme)
    {
        sTheme = theme;
        activity.finish();
        activity.startActivity(new Intent(activity, activity.getClass()));
        activity.overridePendingTransition(android.R.anim.fade_in,
                android.R.anim.fade_out);

        SharedPreferences.Editor editor = getSharedPreferences("theme",MODE_PRIVATE).edit();
        editor.putString("my_theme", String.valueOf(theme));
        editor.apply();

    }

    public void onActivityCreateSetTheme(Activity activity)
    {
        //sTheme = Integer.parseInt(theme);
        switch (sTheme)
        {
            default:
            case THEME_DEFAULT:
                activity.setTheme(R.style.Day);
                break;
            case THEME_NIGHT:
                activity.setTheme(R.style.Night);
                break;

        }
    }

}
